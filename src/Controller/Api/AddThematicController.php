<?php

namespace App\Controller\Api;

use App\Entity\Thematic;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Security;

final class AddThematicController
{
   
    private $security;

    public function __construct( Security $security)
    {

        $this->security=$security;
    }

    public function __invoke(Request $request): Thematic
    {
        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile) {
            throw new BadRequestHttpException('"file" is required');
        }

        $thematic = new Thematic();
        $thematic->setDescription($request->request->get('description'));
        $thematic->setName($request->request->get('name'));
        $thematic->setImageFile($uploadedFile);
        $thematic->setAuthor($this->security->getUser());

        return $thematic;
    }
}